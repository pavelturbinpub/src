

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.01.0622 */
/* at Tue Jan 19 05:14:07 2038
 */
/* Compiler settings for NetCenter.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.01.0622 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif /* __RPCNDR_H_VERSION__ */


#ifndef __NetCenter_h_h__
#define __NetCenter_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __INetCenterLUA_FWD_DEFINED__
#define __INetCenterLUA_FWD_DEFINED__
typedef interface INetCenterLUA INetCenterLUA;

#endif 	/* __INetCenterLUA_FWD_DEFINED__ */


#ifndef __NetCenterLUA_FWD_DEFINED__
#define __NetCenterLUA_FWD_DEFINED__

#ifdef __cplusplus
typedef class NetCenterLUA NetCenterLUA;
#else
typedef struct NetCenterLUA NetCenterLUA;
#endif /* __cplusplus */

#endif 	/* __NetCenterLUA_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __NetCenterLUALib_LIBRARY_DEFINED__
#define __NetCenterLUALib_LIBRARY_DEFINED__

/* library NetCenterLUALib */
/* [helpstring][version][uuid] */ 



EXTERN_C const IID LIBID_NetCenterLUALib;

#ifndef __INetCenterLUA_INTERFACE_DEFINED__
#define __INetCenterLUA_INTERFACE_DEFINED__

/* interface INetCenterLUA */
/* [object][oleautomation][nonextensible][dual][helpstring][uuid] */ 


EXTERN_C const IID IID_INetCenterLUA;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3727C4E2-8EFA-4382-8D81-D7AD506A7FBE")
    INetCenterLUA : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartNetprofmService( void) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct INetCenterLUAVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INetCenterLUA * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INetCenterLUA * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INetCenterLUA * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INetCenterLUA * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INetCenterLUA * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INetCenterLUA * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INetCenterLUA * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartNetprofmService )( 
            INetCenterLUA * This);
        
        END_INTERFACE
    } INetCenterLUAVtbl;

    interface INetCenterLUA
    {
        CONST_VTBL struct INetCenterLUAVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INetCenterLUA_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INetCenterLUA_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INetCenterLUA_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INetCenterLUA_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INetCenterLUA_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INetCenterLUA_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INetCenterLUA_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INetCenterLUA_StartNetprofmService(This)	\
    ( (This)->lpVtbl -> StartNetprofmService(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INetCenterLUA_INTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_NetCenterLUA;

#ifdef __cplusplus

class DECLSPEC_UUID("C0DCC3A6-BE26-4BAD-9833-61DFACE1A8DB")
NetCenterLUA;
#endif
#endif /* __NetCenterLUALib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


