// Demonstrates using of CoGetObject for COM elevation
// Copyright (C) Pavel Turbin, 2017.

#include "stdafx.h"
#include "NetCenter_h.h"
#include "NetCenter_i.c"
#include <string>

int main()
{
    CoInitializeEx(0, COINIT_MULTITHREADED);

    BIND_OPTS3 opt;
    memset(&opt, 0, sizeof(BIND_OPTS3));
    opt.cbStruct = sizeof(BIND_OPTS3);
    opt.dwClassContext = CLSCTX_LOCAL_SERVER; // only local server COM as not running in current application

    WCHAR  UUID[64];
    StringFromGUID2(CLSID_NetCenterLUA, UUID, _countof(UUID));

    std::wstring elevatedUUID = L"Elevation:Administrator!new:"; // prefix to require elevation
    elevatedUUID += UUID; 
    
    INetCenterLUA *pNetCenter = 0;

    HRESULT res = CoGetObject(
            elevatedUUID.c_str(),
            (BIND_OPTS*)&opt,
            __uuidof(INetCenterLUA),
            (PVOID*)&pNetCenter);

    if (res == S_OK)
    {
        // successfully elevated now can call priviledged method
        res = pNetCenter->StartNetprofmService();
    }

    return 0;
}

